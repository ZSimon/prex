package com.prex.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Classname PrexAuthApplication
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-09 10:11
 * @Version 1.0
 */

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class PrexAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrexAuthApplication.class, args);
    }

}
