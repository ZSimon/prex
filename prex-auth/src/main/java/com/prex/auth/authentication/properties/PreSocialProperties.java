package com.prex.auth.authentication.properties;

import lombok.Data;

/**
 * @author lihaodong
 */
@Data
public class PreSocialProperties {

    /**
     * 默认认证页面
     */
    private String filterProcessesUrl = "/social";
    private QQProperties qq = new QQProperties();
    private GithubProperties github = new GithubProperties();
    private GiteeProperties gitee = new GiteeProperties();
    private WeiXinProperties weiXin = new WeiXinProperties();


}
