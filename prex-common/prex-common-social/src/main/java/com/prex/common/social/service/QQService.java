package com.prex.common.social.service;

import com.prex.common.social.entity.QQUserInfo;

/**
 * @Classname QqService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 18:38
 * @Version 1.0
 */
public interface QQService {

    QQUserInfo getUserInfo(String code);
}
