package com.prex.common.social.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Classname GitHubConfig
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 18:11
 * @Version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "prex.social.github")
public class GitHubConfig extends SocialProperties {

    private String redirectUri;
}
